# https://adventofcode.com/2020/day/3

import math
from typing import List, Tuple


def replicas_needed(area_width: int, area_height: int, step_x: int, step_y: int) -> int:
    return math.ceil(((area_height - 1) / step_y * step_x) / area_width)


def build_terrain(area: List[str], replicas: int) -> List[str]:
    return [x * replicas for x in area]


def get_trajectory(terrain_length: int, step_x: int, step_y: int) -> List[Tuple[int, int]]:
    return [(step_x * i, step_y * i) for i in range(terrain_length)]


def get_obstacles(terrain: List[str]) -> List[Tuple[int, int]]:
    return [(ix, iy) for iy, y in enumerate(terrain) for ix, x in enumerate(y) if x == '#']


def get_obstacles_on_trajectory(area: List[str], slope: Tuple[int, int]) -> List[Tuple[int, int]]:
    step_x = slope[0]
    step_y = slope[1]
    area_height = len(area)
    area_width = len(area[0])

    replicas = replicas_needed(area_width, area_height, step_x, step_y)
    terrain = build_terrain(area, replicas)
    terrain_length = len(terrain)
    trajectory = get_trajectory(terrain_length, step_x, step_y)
    obstacles = get_obstacles(terrain)

    hits = set(trajectory).intersection(set(obstacles))

    return list(sorted(hits))


if __name__ == '__main__':
    with open('../data/3.txt') as f:
        given_area = [line.strip() for line in f]

    slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]

    obstacles_on_trajectories = {slope: get_obstacles_on_trajectory(given_area, slope) for slope in slopes}
    print(len(obstacles_on_trajectories[(3, 1)]))  # 278

    number_of_obstacles_on_trajectories = [len(x) for x in obstacles_on_trajectories.values()]
    print(math.prod(number_of_obstacles_on_trajectories))  # 9709761600
