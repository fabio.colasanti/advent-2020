from typing import List


def waiting_time_for_bus(bus_id: int, timestamp: int) -> int:
    return (timestamp // bus_id + 1) * bus_id - timestamp


def waiting_time(bus_ids: List[str], timestamp: int) -> int:
    first_bus = sorted([(waiting_time_for_bus(int(bus_id), timestamp), int(bus_id)) for bus_id in bus_ids if bus_id != 'x'])[0]

    return first_bus[0] * first_bus[1]


def earliest_timestamp_for_concurrent_departures(bus_ids: List[str], offset: int) -> int:
    highest_bus_id, highest_bus_position = sorted([(int(x), i) for i, x in enumerate(bus_ids) if x != 'x'], reverse=True)[0]
    minute = divmod(offset, highest_bus_id)[0] * highest_bus_id

    buses = sorted([(int(bus_id), i) for i, bus_id in enumerate(bus_ids) if bus_id != 'x'])

    len_buses = len(buses)

    departures = 0

    while departures != len_buses:
        departures = 0

        for bus_id, i in buses:
            if (minute + i - highest_bus_position) % bus_id == 0:
                departures += 1
            else:
                break

        minute += highest_bus_id

    return minute - highest_bus_position - highest_bus_id


if __name__ == '__main__':
    with open('../data/13.txt') as f:
        timestamp = int(f.readline())
        bus_ids = [id for id in f.readline().split(',')]

    waiting_time = waiting_time(bus_ids, timestamp)
    print(waiting_time)  # 3215

    minute = earliest_timestamp_for_concurrent_departures(bus_ids, 1000000000000000)
    print(minute)  #  1001569619313439 (8 min!)

