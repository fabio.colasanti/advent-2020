import copy
from typing import List, Tuple, Dict


def decimal_to_binary(value: int) -> str:
    return bin(value).replace("0b", "")


def binary_to_decimal(value: str) -> int:
    return sum([int(x) * (2 ** i) for i, x in enumerate(reversed(value))])


def apply_mask(binary: str, mask: str, version: int) -> str:
    value = ''
    binary = binary.zfill(len(mask))

    if version == 1:
        value = ''.join([x if x in ['0', '1'] else binary[i] for i, x in enumerate(mask)])

    if version == 2:
        value = ''.join([x if x in ['X', '1'] else binary[i] for i, x in enumerate(mask)])

    return value


def initialize_memory(program: List[Tuple[int, int, str]], version: int):
    memory = {}

    if version == 1:
        for address, value, mask in program:
            memory[address] = binary_to_decimal(apply_mask(decimal_to_binary(value), mask, 1))

    if version == 2:
        for address, value, mask in program:
            for address in get_permutations(apply_mask(decimal_to_binary(address), mask, 2)):
                memory[binary_to_decimal(address)] = value

    return memory


def get_permutations(value: str) -> List[str]:
    permutations = []

    indices_x = [i for i, x in enumerate(value) if x == 'X']

    for new_digits in [decimal_to_binary(x).zfill(len(indices_x)) for x in range(2 ** len(indices_x))]:

        new_value = copy.deepcopy(value)
        for i, x in enumerate(indices_x):
            new_value = new_value[:x] + new_digits[i] + new_value[x + 1:]

        permutations.append(new_value)

    return permutations


def parse_program(raw_program: List[str]) -> List[Tuple[int, int, str]]:
    program = []
    mask = ''

    for line in raw_program:
        if line[:4] == 'mask':
            mask = line[7:]
        else:
            address = int(line.split(' = ')[0][4:-1])
            value = int(line.split(' = ')[1])

            program.append((address, value, mask))

    return program


if __name__ == '__main__':
    with open('../data/14.txt') as f:
        raw_program = [line.strip() for line in f]

    program = parse_program(raw_program)

    memory = initialize_memory(program, 1)
    print(sum([x for x in memory.values()]))  # 10452688630537

    memory = initialize_memory(program, 2)
    print(sum([x for x in memory.values()]))  # 2881082759597
