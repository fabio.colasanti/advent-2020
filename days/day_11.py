import copy
from typing import List, Tuple


def count(seats: List[str]) -> int:
    counter = 0

    for i in range(len(seats)):

        if seats[i] == '#':
            counter += 1
            break

        if seats[i] == 'L':
            break

    return counter


def count_occupied_seats_around(layout: List[List[str]], seat: Tuple[int, int], adjacent: bool) -> int:
    up_right = []
    down_right = []
    up_left = []
    down_left = []

    left = layout[seat[1]][:seat[0]]
    right = layout[seat[1]][seat[0] + 1:]
    up = [x[seat[0]] for x in layout][:seat[1]]
    down = [x[seat[0]] for x in layout][seat[1] + 1:]

    i = 1
    while seat[0] - i >= 0 and seat[1] - i >= 0:
        seat_type = layout[seat[1] - i][seat[0] - i]
        up_left.append(seat_type)
        i += 1

        if seat_type == '#':
            break

    i = 1
    while seat[0] + i <= len(layout[0]) - 1 and seat[1] + i <= len(layout) - 1:
        seat_type = layout[seat[1] + i][seat[0] + i]
        down_right.append(seat_type)
        i += 1

        if seat_type == '#':
            break

    i = 1
    while seat[0] + i <= len(layout[0]) - 1 and seat[1] - i >= 0:
        seat_type = layout[seat[1] - i][seat[0] + i]
        up_right.append(seat_type)
        i += 1

        if seat_type == '#':
            break

    i = 1
    while seat[0] - i >= 0 and seat[1] + i <= len(layout) - 1:
        seat_type = layout[seat[1] + i][seat[0] - i]
        down_left.append(seat_type)
        i += 1

        if seat_type == '#':
            break

    up.reverse()
    left.reverse()

    if adjacent:
        return sum([1 if x[0] == '#' else 0 for x in [up, up_right, right, down_right, down, down_left, left, up_left] if x])

    else:
        return count(up) + count(up_right) + count(right) + count(down_right) + count(down) + count(down_left) + count(left) + count(up_left)


def change_state(layout: List[List[str]], adjacent: bool, min_occupied_seats_to_flip: int) -> Tuple[List[List[str]], bool]:
    new_layout = copy.deepcopy(layout)
    changed = False

    for x in range(0, len(layout[0])):
        for y in range(0, len(layout)):
            seat = (x, y)

            if layout[y][x] == 'L' and count_occupied_seats_around(layout, seat, adjacent) == 0:
                new_layout[y][x] = '#'
                changed = True

            if layout[y][x] == '#' and count_occupied_seats_around(layout, seat, adjacent) >= min_occupied_seats_to_flip:
                new_layout[y][x] = 'L'
                changed = True

    return new_layout, changed


def reassign_seats(layout: List[List[str]], adjacent, min_occupied_seats_to_flip: int) -> List[List[str]]:
    changed = True

    while changed:
        layout, changed = change_state(layout, adjacent, min_occupied_seats_to_flip)

    return layout


def count_seats_by_type(layout: List[List[str]], seat_type: str) -> int:
    counter = 0

    for x in range(0, len(layout[0])):
        for y in range(0, len(layout)):
            if layout[y][x] == seat_type:
                counter += 1

    return counter


if __name__ == '__main__':
    given_layout = []
    with open('../data/11.txt') as f:
        for line in f:
            row = [value for value in line if value != '\n']
            given_layout.append(row)

    new_layout = reassign_seats(given_layout, True, 4)
    occupied_seats = count_seats_by_type(new_layout, '#')
    print(occupied_seats)  # 2249

    new_layout = reassign_seats(given_layout, False, 5)
    occupied_seats = count_seats_by_type(new_layout, '#')
    print(occupied_seats)  # 2023
