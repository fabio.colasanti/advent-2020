import re


def solve(expression: str) -> int:
    parenthesis = re.search('\(([^()]*)\)', expression)
    if parenthesis:
        start, end = parenthesis.regs[0]

        new_expression = expression[:start] + str(solve(expression[start + 1:end - 1])) + expression[end:]
        return solve(new_expression)

    else:
        values = expression.split(' ')
        if len(values) >= 3:

            if values[1] == '+':
                result = int(values[0]) + int(values[2])

            if values[1] == '*':
                result = int(values[0]) * int(values[2])

            new_expression = str(result) + ' ' + ' '.join(values[3:])
            return solve(new_expression)

        else:
            return int(values[0])


def solve_advanced(expression: str) -> int:
    parenthesis = re.search('\(([^()]*)\)', expression)
    if parenthesis:
        start, end = parenthesis.regs[0]

        new_expression = expression[:start] + str(solve_advanced(expression[start + 1:end - 1])) + expression[end:]
        return solve_advanced(new_expression)

    else:
        values = expression.split(' ')
        if len(values) >= 3:

            while len([i for i, x in enumerate(values) if x == '+']) > 0:
                i = min([i for i, x in enumerate(values) if x == '+'])
                values = values[:i - 1] + [str(int(values[i - 1]) + int(values[i + 1]))] + values[i + 2:]

            while len([i for i, x in enumerate(values) if x == '*']) > 0:
                i = min([i for i, x in enumerate(values) if x == '*'])
                values = values[:i - 1] + [str(int(values[i - 1]) * int(values[i + 1]))] + values[i + 2:]

        return int(values[0])


if __name__ == '__main__':
    with open('../data/18.txt') as f:
        expressions = [line.strip() for line in f]

    sum_of_results = sum([solve(x) for x in expressions])
    print(sum_of_results)  # 30753705453324

    sum_of_results = sum([solve_advanced(x) for x in expressions])
    print(sum_of_results)  # 244817530095503
