from typing import Tuple


def binarise_boarding_pass(boarding_pass: str) -> str:
    return boarding_pass.replace('L', '0').replace('R', '1').replace('F', '0').replace('B', '1')


def get_coordinate(binary_seat: str, dimension: int, offset: int = 0) -> int:
    candidate_seats = [x + offset for x in range(dimension)]

    if len(candidate_seats) > 1:
        if binary_seat[0] == '0':
            return get_coordinate(binary_seat[1:], int(dimension / 2), offset)
        else:
            return get_coordinate(binary_seat[1:], int(dimension / 2), offset + int(dimension / 2))
    else:
        return candidate_seats[0]


def get_seat_coordinates(boarding_pass: str, aircraft_dimensions) -> Tuple[int, int]:
    binary_boarding_pass = binarise_boarding_pass(boarding_pass)

    rows_partitioning = binary_boarding_pass[:7]
    cols_partitioning = binary_boarding_pass[7:]

    row = get_coordinate(rows_partitioning, aircraft_dimensions[0])
    col = get_coordinate(cols_partitioning, aircraft_dimensions[1])

    return row, col


def get_seat_id(seat_coordinates):
    return seat_coordinates[0] * 8 + seat_coordinates[1]


if __name__ == '__main__':
    with open('../data/5.txt') as f:
        given_seats = [line.strip() for line in f]

    aircraft_dimensions = (128, 8)
    seats_coordinates = [get_seat_coordinates(seat, aircraft_dimensions) for seat in given_seats]
    seats_ids = [get_seat_id(seat) for seat in seats_coordinates]

    print(max(seats_ids))  # 818

    free_seat = [x for x in range(818) if x >= min(seats_ids) and x not in seats_ids][0]
    print(free_seat)  # 818
