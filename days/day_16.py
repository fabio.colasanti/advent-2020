import math
from typing import List, Tuple, Dict


def parse_notes(notes: List[str]) -> Tuple[Dict[str, List[int]], List[int], List[List[int]]]:
    number_of_rules = notes.index('')
    my_ticket_index = number_of_rules + 2
    nearby_tickets_index = number_of_rules + 5
    number_of_nearby_tickets = len(notes) - nearby_tickets_index

    rules = {}
    my_ticket = [int(x) for x in notes[my_ticket_index].split(',')]
    nearby_tickets = []

    for i in range(number_of_rules):
        key, ranges = notes[i].split(':')

        value = []
        for r in ranges.split(' or '):
            start, end = r.split('-')

            [value.append(x) for x in range(int(start), int(end) + 1)]

        rules[key] = value

    for i in range(nearby_tickets_index, nearby_tickets_index + number_of_nearby_tickets):
        ticket = [int(x) for x in notes[i].split(',')]
        nearby_tickets.append(ticket)

    return rules, my_ticket, nearby_tickets


def error_in_ticket(ticket: List[int], rules: Dict[str, List[int]]) -> int:
    valid_values = set([value for values in rules.values() for value in values])

    for value in ticket:
        if value not in valid_values:
            return value


def tickets_validation(tickets: List[List[int]], rules: Dict[str, List[int]]) -> Tuple[List[List[int]], List[int]]:
    valid_tickets = []
    errors = []

    for ticket in tickets:
        error = error_in_ticket(ticket, rules)

        if error is not None:
            errors.append(error)
        else:
            valid_tickets.append(ticket)

    return valid_tickets, errors


def get_rules_mapping(tickets: List[List[int]], rules: Dict[str, List[int]]) -> List[int]:
    result = []
    viable_options = []
    no_of_columns = len(tickets[0])

    for rule_index, rule_values in enumerate(rules.values()):
        candidates = []

        for column in range(no_of_columns):
            ticket_values = [ticket[column] for ticket in tickets]

            if set(rule_values).issuperset(set(ticket_values)):
                candidates.append(column)

        viable_options.append((rule_index, candidates))

    for i in range(no_of_columns):
        unique_match = [(x[0], x[1][0]) for x in viable_options if len(x[1]) == 1][0]
        result.append(unique_match)

        [x[1].remove(unique_match[1]) for x in viable_options if x[1]]

    ordered_rules = [x[1] for x in sorted(result)]
    return ordered_rules


if __name__ == '__main__':
    with open('../data/16.txt') as f:
        rules, my_ticket, nearby_tickets = parse_notes([line.strip() for line in f])

    nearby_tickets.append(my_ticket)
    valid_tickets, errors = tickets_validation(nearby_tickets, rules)
    print(sum(errors))  # 23009

    rules_mapping = list(zip(rules.keys(), get_rules_mapping(valid_tickets, rules)))
    print(math.prod([my_ticket[i] for i, x in enumerate(rules_mapping) if x[0].startswith('departure')]))  # 10458887314153
