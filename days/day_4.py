# https://adventofcode.com/2020/day/3

import re
from typing import List, Dict


def get_passports(batch: List[str]) -> List[Dict]:
    passports = []

    for item in batch:
        passport = {x[:3]: x[4:] for x in item.split(' ')}
        passports.append(passport)

    return passports


def discard_invalid_passports(passports: List[Dict], keys_only: bool = False) -> List[Dict]:
    if keys_only:
        return [p for p in passports if validate_passport_keys(p)]
    else:
        return [p for p in passports if validate_passport(p)]


def validate_passport_keys(passport: Dict) -> bool:
    required_fields = ('ecl', 'pid', 'eyr', 'hcl', 'byr', 'iyr', 'hgt')

    return set(passport.keys()).issuperset(required_fields)


def validate_passport(passport: Dict) -> bool:
    try:
        if not 1920 <= int(passport.get('byr')) <= 2002:
            return False

        if not 2010 <= int(passport.get('iyr')) <= 2020:
            return False

        if not 2020 <= int(passport.get('eyr')) <= 2030:
            return False

        if not len(passport.get('pid')) == 9:
            return False

        if not re.match("^#([a-f0-9]{6})$", passport.get('hcl')):
            return False

        if not passport.get('ecl') in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
            return False

        if not passport.get('hgt')[-2:] in ['cm', 'in']:
            return False

        if passport.get('hgt')[-2:] == 'cm':
            if not 150 <= int(passport.get('hgt')[:-2]) <= 193:
                return False

        if passport.get('hgt')[-2:] == 'in':
            if not 59 <= int(passport.get('hgt')[:-2]) <= 76:
                return False

    except:
        return False

    return True


if __name__ == '__main__':
    with open('../data/4.txt') as f:
        given_batch = [doc.replace('\n', ' ') for doc in f.read().split('\n\n')]

    passports = get_passports(given_batch)

    valid_passports = discard_invalid_passports(passports, keys_only=True)
    print(len(valid_passports))  # 256

    valid_passports = discard_invalid_passports(passports)
    print(len(valid_passports))  # 198
