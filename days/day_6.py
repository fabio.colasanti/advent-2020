from functools import reduce


def count_yes_from_anyone_group(group: str) -> int:
    return len(set(group.replace(' ', '')))


def count_yes_from_everyone_group(group: str) -> int:
    return len(reduce(lambda x, y: set(x) & set(y), group.split()))


if __name__ == '__main__':
    with open('../data/6.txt') as f:
        given_answers = [doc.replace('\n', ' ') for doc in f.read().split('\n\n')]

    yes_from_anyone_group = [count_yes_from_anyone_group(a) for a in given_answers]
    print(sum(yes_from_anyone_group))  # 6273

    yes_from_everyone_group = [count_yes_from_everyone_group(a) for a in given_answers]
    print(sum(yes_from_everyone_group))  # 3254
