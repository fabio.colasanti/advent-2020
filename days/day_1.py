# https://adventofcode.com/2020/day/1

import math
from copy import copy
from typing import List


def get_m_entries_to_sum_n(expenses: List, m: int, n: int) -> List:
    entries = []

    if m == 1:
        if n in expenses:
            entries.append(n)
    else:
        while expenses:
            em = expenses.pop()
            entries = get_m_entries_to_sum_n(copy(expenses), m - 1, n - em)

            if entries:
                entries.append(em)
                break

    return entries


if __name__ == '__main__':
    with open('../data/1.txt') as f:
        given_expenses = [int(line.strip()) for line in f]

    entries_to_multiply = get_m_entries_to_sum_n(copy(given_expenses), 2, 2020)
    print(math.prod(entries_to_multiply))  # 1007331

    entries_to_multiply = get_m_entries_to_sum_n(copy(given_expenses), 3, 2020)
    print(math.prod(entries_to_multiply))  # 48914340
