from typing import List


def is_valid(seq: List[int], num: int) -> bool:
    for x in seq:
        diff = num - x
        if diff in seq and x != diff:
            return True

    return False


def get_first_invalid_num(series: List[int], preamble_size: int) -> int:
    for i in range(len(series) - preamble_size - 1):
        seq = series[i:preamble_size + i]
        num = series[preamble_size + i]

        if not is_valid(seq, num):
            return num

    return 0


def get_contiguous_range_adding_to_num(series: List[int], num: int) -> List[int]:
    for i in range(len(series)):
        contiguous_range = []
        tot = 0
        j = 0

        while tot < num:
            value = series[i + j]

            tot += value
            contiguous_range.append(value)

            j += 1

        if tot == num:
            return contiguous_range

    return []


if __name__ == '__main__':
    with open('../data/9.txt') as f:
        given_series = [int(line) for line in f]

    invalid_num = get_first_invalid_num(given_series, 25)
    print(invalid_num)  # 1309761972

    contiguous_range = get_contiguous_range_adding_to_num(given_series, invalid_num)
    weakness = max(contiguous_range) + min(contiguous_range)
    print(weakness)  # 177989832
