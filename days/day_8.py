import copy
from typing import Tuple, List


def execute_instruction(instruction: Tuple[str, int], accumulator: int, pointer: int) -> Tuple[int, int]:
    command = instruction[0]
    value = instruction[1]

    if command == 'nop':
        pointer += 1

    if command == 'jmp':
        pointer += value

    if command == 'acc':
        pointer += 1
        accumulator += value

    return accumulator, pointer


def run_program(program: List[Tuple[str, int]]) -> Tuple[int, bool]:
    executed = []
    pointer = 0
    accumulator = 0
    broken = True

    while pointer not in executed:
        if pointer == len(program):
            broken = False
            break

        instruction = program[pointer]
        executed.append(pointer)
        accumulator, pointer = execute_instruction(instruction, accumulator, pointer)

    return accumulator, broken


def fix_program(program: List[Tuple[str, int]]) -> int:
    broken = True
    accumulator = 0

    for i, instruction in enumerate(program):
        command = instruction[0]
        value = instruction[1]

        if command == 'nop':
            variant = copy.copy(program)
            variant[i] = ('jmp', value)

            accumulator, broken = run_program(variant)

        if command == 'jmp':
            variant = copy.copy(program)
            variant[i] = ('nop', value)

            accumulator, broken = run_program(variant)

        if not broken:
            break

    return accumulator


if __name__ == '__main__':
    with open('../data/8.txt') as f:
        given_program = [(line[:3], int(line[4:])) for line in f]

    last_accumulator, _ = run_program(given_program)
    print(last_accumulator)  # 1614

    last_accumulator = fix_program(given_program)
    print(last_accumulator)  # 1260
