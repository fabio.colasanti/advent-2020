import math
from typing import List


def get_adapters_diffs(ratings: List[int], outlet_joltage: int) -> List[int]:
    ratings.append(outlet_joltage)
    ratings.append(max(ratings) + 3)
    ratings.sort()

    return [rating - ratings[i - 1] for i, rating in enumerate(ratings)][1:]


def tribonacci(n):
    series = [0, 1, 1]
    for i in range(n - 1):
        series.append(sum(series[-3:]))

    return series[n + 1]


def get_chuncks(sequence: List[int], separator: int = 3) -> List[List[int]]:
    chuncks = []
    start = 0

    for i, diff in enumerate(sequence):
        if diff == separator:
            chuncks.append(sequence[start:i])
            start = i + 1

    return chuncks


def get_combinations(adapters_diffs: List[int]) -> List[List[int]]:
    chuncks = get_chuncks(adapters_diffs)

    return math.prod([tribonacci(len(chunck)) for chunck in chuncks])


if __name__ == '__main__':
    with open('../data/10.txt') as f:
        given_ratings = [int(line) for line in f]

    adapters_diffs = get_adapters_diffs(given_ratings, 0)

    one_j_diffs = len([x for x in adapters_diffs if x == 1])
    three_j_diffs = len([x for x in adapters_diffs if x == 3])

    print(one_j_diffs * three_j_diffs)  # 2590

    combinations = get_combinations(adapters_diffs)
    print(combinations)
