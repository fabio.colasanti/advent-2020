class Waypoint(object):
    def __init__(self, position):
        self.position_x = position[0]
        self.position_y = position[1]

    def rotate(self, rotation, value):
        quarters = value // 90
        for i in range(quarters):
            if self.position_x * self.position_y > 0:
                new_x = self.position_y
                new_y = - self.position_x
            else:
                new_x = self.position_y
                new_y = - self.position_x

            if rotation == 'L':
                new_x = - new_x
                new_y = - new_y

            self.position_x = new_x
            self.position_y = new_y

    def shift(self, direction, value):
        if direction == 'N':
            self.position_y += value

        if direction == 'E':
            self.position_x += value

        if direction == 'S':
            self.position_y -= value

        if direction == 'W':
            self.position_x -= value


class Ship(object):
    def __init__(self, position, orientation):
        self.position_x = position[0]
        self.position_y = position[1]
        self.orientation = orientation

    def rotate(self, rotation, value):
        orientations = ['N', 'E', 'S', 'W']
        quarters = value // 90

        orientation_index = orientations.index(self.orientation)
        increment = divmod(quarters, 4)[1]

        if rotation == 'R':
            self.orientation = orientations[divmod(orientation_index + increment, 4)[1]]

        if rotation == 'L':
            self.orientation = orientations[divmod(orientation_index - increment, 4)[1]]

    def shift(self, direction, value):
        if direction == 'N':
            self.position_y += value

        if direction == 'E':
            self.position_x += value

        if direction == 'S':
            self.position_y -= value

        if direction == 'W':
            self.position_x -= value


def move(ship, command, waypoint=None):
    command_type = command[:1]
    command_value = int(command[1:])

    if command_type == 'F':
        if waypoint:
            ship.shift('E', waypoint.position_x * command_value)
            ship.shift('N', waypoint.position_y * command_value)
        else:
            ship.shift(ship.orientation, command_value)

    if command_type in ['L', 'R']:
        if waypoint:
            waypoint.rotate(command_type, command_value)
        else:
            ship.rotate(command_type, command_value)

    if command_type in ['N', 'E', 'S', 'W']:
        if waypoint:
            waypoint.shift(command_type, command_value)
        else:
            ship.shift(command_type, command_value)


if __name__ == '__main__':
    with open('../data/12.txt') as f:
        commands = [line for line in f]

    for waypoint in [None, Waypoint(position=(10, 1))]:
        ship = Ship(position=(0, 0), orientation='E')

        [move(ship, command, waypoint) for command in commands]

        manhattan_distance_from_origin = abs(ship.position_x) + abs(ship.position_y)
        print(manhattan_distance_from_origin)  # 1687 / 20873
