from typing import List


def number_at_turn(seq: List[int], turn: int) -> int:
    last_seen = {x: i for i, x in enumerate(seq[:-1])}
    seq_len = len(seq)

    while turn > seq_len:
        value = seq[-1]

        if value not in last_seen.keys():
            next = 0
        else:
            next = seq_len - last_seen.get(value) - 1

        last_seen[value] = seq_len - 1
        seq.append(next)
        seq_len += 1

    return seq[-1]


if __name__ == '__main__':
    seq = [0, 12, 6, 13, 20, 1, 17]

    print(number_at_turn(seq, 2020))  # 620
    print(number_at_turn(seq, 300000))  # 110871
