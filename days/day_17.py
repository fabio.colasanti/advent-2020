from typing import Tuple, Dict, List


def parse_pocket(raw_pocket: List[str]) -> Dict[Tuple[int, int, int, int], str]:
    pocket = {}
    z = 0
    w = 0

    for y, values in enumerate(raw_pocket):
        for x, value in enumerate(values):
            pocket[(x, y, z, w)] = value

    return pocket


def count_active_neighbours(cube: Tuple[int, int, int, int], state: Dict[Tuple[int, int, int, int], str]) -> int:
    neighbours = []

    for w in range(cube[3] - 1, cube[3] + 2):
        for z in range(cube[2] - 1, cube[2] + 2):
            for y in range(cube[1] - 1, cube[1] + 2):
                for x in range(cube[0] - 1, cube[0] + 2):
                    if (x, y, z, w) != cube:
                        neighbours.append(state.get((x, y, z, w), '.'))

    return neighbours.count('#')


def change_state(cube: Tuple[int, int, int, int], state: Dict[Tuple[int, int, int, int], str]) -> str:
    active_neighbours = count_active_neighbours(cube, state)

    if state.get(cube, '.') == '#':
        if active_neighbours in [2, 3]:
            return '#'
        else:
            return '.'

    if state.get(cube, '.') == '.':
        if active_neighbours == 3:
            return '#'
        else:
            return '.'


def boot_cycle(state: Dict[Tuple[int, int, int, int], str]) -> Dict[Tuple[int, int, int, int], str]:
    new_state = {}

    x_range = [k[0] for k in state.keys()]
    y_range = [k[1] for k in state.keys()]
    z_range = [k[2] for k in state.keys()]
    w_range = [k[3] for k in state.keys()]

    for w in range(min(w_range) - 1, max(w_range) + 2):
        for z in range(min(z_range) - 1, max(z_range) + 2):
            for y in range(min(y_range) - 1, max(y_range) + 2):
                for x in range(min(x_range) - 1, max(x_range) + 2):
                    new_state[(x, y, z, w)] = change_state((x, y, z, w), state)

    return new_state


if __name__ == '__main__':
    with open('../data/17.txt') as f:
        raw_pocket = [line.strip() for line in f]

    state = parse_pocket(raw_pocket)

    for cycle in range(6):
        state = boot_cycle(state)

    active_cubes = sum([1 for value in state.values() if value == '#'])
    print(active_cubes)  # 255 #2340
