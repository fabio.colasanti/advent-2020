from typing import Dict, List


def parse_rule(rule: str) -> Dict:
    container, contents = rule.replace('.\n', '').split('contain')

    key = container.replace('bags', '').strip()

    if contents.strip() != 'no other bags':
        values = [(x.replace('bags', '').replace('bag', '').strip()[2:], int(x.strip()[:1])) for x in contents.split(',')]
    else:
        values = []

    return {key: values}


def get_containers(ruleset: List[Dict], color: str) -> List:
    containers = []
    parents = []

    for rule in ruleset:
        if color in [x[0] for items in rule.values() for x in items]:
            parents.append(list(rule.keys())[0])

    for p in parents:
        containers.append(p)
        [containers.append(x) for x in get_containers(ruleset, p)]

    return list(set(containers))


def count_contents(ruleset: List[Dict], color: str) -> int:
    count = 0

    for rule in ruleset:
        if color in list(rule.keys()):
            for content in list(rule.values())[0]:
                count += content[1] + content[1] * count_contents(ruleset, content[0])

    return count


if __name__ == '__main__':
    with open('../data/7.txt') as f:
        given_ruleset = [parse_rule(line) for line in f]

    containers = get_containers(given_ruleset, 'shiny gold')
    print(len(containers))  # 296

    contents = count_contents(given_ruleset, 'shiny gold')
    print(contents)  # 9339
