# https://adventofcode.com/2020/day/2

from typing import List


def get_valid_passwords_old_system(passwords: List[str]) -> List:
    valid = []

    for p in passwords:
        range, char, value = p.replace(':', '').split()
        min, max = [int(x) for x in range.split('-')]

        if min <= value.count(char) <= max:
            valid.append(value)

    return valid


def get_valid_passwords_new_system(passwords: List[str]) -> List:
    valid = []

    for p in passwords:
        positions, char, value = p.replace(':', '').split()
        char_indices = [str(i + 1) for i, x in enumerate(value) if x == char]
        matching_positions = set(positions.split('-')).intersection(set(char_indices))

        if len(matching_positions) == 1:
            valid.append(value)

    return valid


if __name__ == '__main__':
    with open('../data/2.txt') as f:
        given_passwords = [line.strip() for line in f]

    valid_passwords_old_system = get_valid_passwords_old_system(given_passwords)
    print(len(valid_passwords_old_system))  # 424

    valid_passwords_new_system = get_valid_passwords_new_system(given_passwords)
    print(len(valid_passwords_new_system))  # 747
