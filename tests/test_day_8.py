import pytest

from days.day_8 import *


@pytest.mark.parametrize('instruction, accumulator, pointer, expected', [
    (('nop', 0), 0, 0, (0, 1)),
    (('acc', 1), 0, 1, (1, 2)),
    (('jmp', 4), 1, 2, (1, 6)),
])
def test_execute_instruction(instruction, accumulator, pointer, expected):
    new_accumulator, new_pointer = execute_instruction(instruction, accumulator, pointer)

    assert (new_accumulator, new_pointer) == expected


def test_run_broken_program():
    program = [
        ('nop', 0),
        ('acc', 1),
        ('jmp', 4),
        ('acc', 3),
        ('jmp', -3),
        ('acc', -99),
        ('acc', 1),
        ('jmp', -4),
        ('acc', 6)
    ]

    expected = (5, True)

    actual = run_program(program)

    assert actual == expected


def test_run_sound_program():
    program = [
        ('nop', 0),
        ('acc', 1),
        ('jmp', 4),
        ('acc', 3),
        ('jmp', -3),
        ('acc', -99),
        ('acc', 1),
        ('nop', -4),
        ('acc', 6)
    ]

    expected = (8, False)

    actual = run_program(program)

    assert actual == expected


def test_fix_program():
    program = [
        ('nop', 0),
        ('acc', 1),
        ('jmp', 4),
        ('acc', 3),
        ('jmp', -3),
        ('acc', -99),
        ('acc', 1),
        ('jmp', -4),
        ('acc', 6)
    ]

    expected = 8

    actual = fix_program(program)

    assert actual == expected
