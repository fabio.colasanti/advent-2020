import pytest

from days.day_16 import parse_notes, error_in_ticket, tickets_validation, get_rules_mapping


def test_parse_notes():
    notes = [
        'class: 1-3 or 5-7',
        'row: 6-11 or 33-44',
        'seat: 13-40 or 45-50',
        '',
        'your ticket:',
        '7,1,14',
        '',
        'nearby tickets:',
        '7,3,47',
        '40,4,50',
        '55,2,20',
        '38,6,12'
    ]

    expected_rules = {
        'class': [1, 2, 3, 5, 6, 7],
        'row': [6, 7, 8, 9, 10, 11, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44],
        'seat': [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
                 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 47, 48, 49, 50]
    }

    expected_my_ticket = [7, 1, 14]
    expected_nearby_tickets = [
        [7, 3, 47],
        [40, 4, 50],
        [55, 2, 20],
        [38, 6, 12]
    ]

    rules, my_ticket, nearby_tickets = parse_notes(notes)

    assert rules == expected_rules
    assert my_ticket == expected_my_ticket
    assert nearby_tickets == expected_nearby_tickets


@pytest.mark.parametrize('ticket, expected', [
    ([7, 3, 47], None),
    ([40, 4, 50], 4),
    ([55, 2, 20], 55),
    ([38, 6, 12], 12)
])
def test_error_in_ticket(ticket, expected):
    rules = {
        'class': [1, 2, 3, 5, 6, 7],
        'row': [6, 7, 8, 9, 10, 11, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44],
        'seat': [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
                 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 47, 48, 49, 50]
    }

    actual = error_in_ticket(ticket, rules)

    assert actual == expected


def test_bulk_validation():
    tickets = [
        [7, 3, 47],
        [40, 4, 50],
        [55, 2, 20],
        [38, 6, 12]
    ]

    rules = {
        'class': [1, 2, 3, 5, 6, 7],
        'row': [6, 7, 8, 9, 10, 11, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44],
        'seat': [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
                 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 47, 48, 49, 50]
    }

    expected_valid_tickets = [[7, 3, 47]]
    expected_errors = [4, 55, 12]

    actual_valid_tickets, actual_errors = tickets_validation(tickets, rules)

    assert actual_valid_tickets == expected_valid_tickets
    assert actual_errors == expected_errors


def test_get_rules_mapping():
    tickets = [
        [3, 9, 18],
        [15, 1, 5],
        [5, 14, 9]
    ]

    rules = {
        'class': [0, 1, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
        'row': [0, 1, 2, 3, 4, 5, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
        'seat': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16, 17, 18, 19]
    }

    expected = [1, 0, 2]

    actual = get_rules_mapping(tickets, rules)

    assert actual == expected
