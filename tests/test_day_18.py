import pytest

from days.day_18 import solve, solve_advanced


@pytest.mark.parametrize('expression, expected', [
    ('1 + 2', 3),
    ('12 * 2', 24),
    ('8 * 3 + 9 + 3 * 4 * 3', 432),
    ('2 * (3 + 5)', 16),
    ('2 * 3 + (4 * 5)', 26),
    ('5 + (8 * 3 + 9 + 3 * 4 * 3)', 437),
    ('5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))', 12240),
    ('((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2', 13632)
])
def test_solve(expression, expected):
    actual = solve(expression)

    assert actual == expected


@pytest.mark.parametrize('expression, expected', [
    ('1 + 2 * 3 + 4 * 5 + 6', 231),
    ('1 + (2 * 3) + (4 * (5 + 6))', 51),
    ('2 * 3 + (4 * 5)', 46),
    ('5 + (8 * 3 + 9 + 3 * 4 * 3)', 1445),
    ('5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))', 669060),
    ('((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2', 23340)
])
def test_solve_advanced(expression, expected):
    actual = solve_advanced(expression)

    assert actual == expected
