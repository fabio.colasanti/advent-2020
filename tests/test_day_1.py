import pytest

from days.day_1 import get_m_entries_to_sum_n


@pytest.mark.parametrize('m', [1, 2, 3, 4])
def test_get_m_entries_to_sum_n(m):
    n = 2020
    expenses = [1721, 979, 366, 299, 675, 1456, 1000, 100, 900, 20, 2020]

    actual = get_m_entries_to_sum_n(expenses, m, n)

    assert sum(actual) == n
