import pytest

from days.day_13 import *


@pytest.mark.parametrize('bus_id, expected', [
    (7, 6),
    (13, 10),
    (59, 5),
])
def test_waiting_time_for_bus(bus_id, expected):
    timestamp = 939

    actual = waiting_time_for_bus(bus_id, timestamp)

    assert actual == expected


def test_waiting_time():
    timestamp = 939
    bus_ids = ['7', '13', 'x', 'x', '59', 'x', '31', '19']

    expected = 295

    actual = waiting_time(bus_ids, timestamp)

    assert actual == expected


@pytest.mark.parametrize('bus_ids, offset, expected', [
    (['7', '13', 'x', 'x', '59', 'x', '31', '19'], 1000000, 1068781),
    (['67', '7', '59', '61'], 700000, 754018),
    (['67', 'x', '7', '59', '61'], 700000, 779210),
    (['67', '7', 'x', '59', '61'], 700000, 1261476),
    (['1789', '37', '47', '1889'], 2000, 1202161486),
    (['1789', '37', '47', '1889'], 100000000, 1202161486),
])
def test_earliest_timestamp_for_concurrent_departures(bus_ids, offset, expected):
    actual = earliest_timestamp_for_concurrent_departures(bus_ids, offset)

    assert actual == expected
