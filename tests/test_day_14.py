import pytest

from days.day_14 import *


@pytest.mark.parametrize('value, expected', [
    (22, '10110'),
    (11, '1011'),
    (73, '1001001')
])
def test_decimal_to_binary(value, expected):
    actual = decimal_to_binary(value)

    assert actual == expected


@pytest.mark.parametrize('value, expected', [
    ('10110', 22),
    ('1011', 11),
    ('1001001', 73)
])
def test_binary_to_decimal(value, expected):
    actual = binary_to_decimal(value)

    assert actual == expected


@pytest.mark.parametrize('binary, mask, expected', [
    ('0000001011', 'XXX1XXXX0X', '0001001001'),
    ('0001100101', 'XXX1XXXX0X', '0001100101'),
    ('0000000000', 'XXX1XXXX0X', '0001000000')
])
def test_apply_mask_v1(binary, mask, expected):
    actual = apply_mask(binary, mask, 1)

    assert actual == expected


@pytest.mark.parametrize('binary, mask, expected', [
    ('0000101010', '0000X1001X', '0000X1101X'),
    ('0000011010', '000000X0XX', '000001X0XX')
])
def test_apply_mask_v2(binary, mask, expected):
    actual = apply_mask(binary, mask, 2)

    assert actual == expected


def test_initialize_memory_v1():
    program = [
        (8, 11, 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X'),
        (7, 101, 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X'),
        (8, 0, 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X')
    ]

    memory = initialize_memory(program, 1)

    assert memory[7] == 101
    assert memory[8] == 64
    assert sum(x[1] for x in memory.items() if x[0] not in [7, 8]) == 0


def test_initialize_memory_v2():
    program = [
        (42, 100, '000000000000000000000000000000X1001X'),
        (26, 1, '00000000000000000000000000000000X0XX')
    ]

    memory = initialize_memory(program, 2)

    assert memory[26] == 1
    assert memory[58] == 100
    assert sum([x for x in memory.values()]) == 208


@pytest.mark.parametrize('value, expected', [
    ('00X1101X', ['00011010',
                  '00011011',
                  '00111010',
                  '00111011']),
    ('1X0XX', ['10000',
               '10001',
               '10010',
               '10011',
               '11000',
               '11001',
               '11010',
               '11011'])
])
def test_get_permutations(value, expected):
    actual = get_permutations(value)

    assert sorted(actual) == sorted(expected)


def test_parse_program():
    raw_program = [
        'mask = 100110X100000XX0X100X1100110X001X100',
        'mem[21836] = 68949',
        'mem[61020] = 7017251',
        'mask = X00X0011X11000X1010X0X0X110X0X011000',
        'mem[30885] = 231192',
        'mem[26930] = 133991367',
        'mem[1005] = 121034',
        'mem[20714] = 19917',
        'mem[55537] = 9402614'
    ]

    expected = [(21836, 68949, '100110X100000XX0X100X1100110X001X100'),
                (61020, 7017251, '100110X100000XX0X100X1100110X001X100'),
                (30885, 231192, 'X00X0011X11000X1010X0X0X110X0X011000'),
                (26930, 133991367, 'X00X0011X11000X1010X0X0X110X0X011000'),
                (1005, 121034, 'X00X0011X11000X1010X0X0X110X0X011000'),
                (20714, 19917, 'X00X0011X11000X1010X0X0X110X0X011000'),
                (55537, 9402614, 'X00X0011X11000X1010X0X0X110X0X011000')]

    actual = parse_program(raw_program)

    assert actual == expected
