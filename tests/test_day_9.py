import pytest

from days.day_9 import *


@pytest.mark.parametrize('num, expected', [
    (26, True),
    (49, True),
    (100, False),
    (50, False)
])
def test_is_valid(num, expected):
    seq = [x + 1 for x in range(25)]

    actual = is_valid(seq, num)

    assert actual == expected


def test_first_invalid_num():
    series = [35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309, 576]
    preamble_size = 5

    expected = 127

    actual = get_first_invalid_num(series, preamble_size)

    assert actual == expected


def test_get_contiguous_range_adding_to_num():
    series = [35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309, 576]
    num = 127

    expected = [15, 25, 47, 40]

    actual = get_contiguous_range_adding_to_num(series, num)

    assert actual == expected
