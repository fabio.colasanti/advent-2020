import pytest

from days.day_7 import *


@pytest.mark.parametrize('color, expected', [
    ('shiny gold', ['muted yellow', 'bright white', 'light red', 'dark orange']),
    ('faded blue', ['muted yellow', 'light red', 'dark orange']),
    ('bright white', ['light red', 'dark orange'])
])
def test_get_containers(color, expected):
    ruleset = [
        {
            'light red': [
                ('bright white', 1),
                ('muted yellow', 2)
            ]
        },
        {
            'dark orange': [
                ('bright white', 3),
                ('muted yellow', 4)
            ]
        },
        {
            'bright white': [
                ('shiny gold', 1)
            ]
        },
        {
            'muted yellow': [
                ('shiny gold', 2),
                ('faded blue', 9)
            ]
        }
    ]

    actual = get_containers(ruleset, color)

    assert sorted(actual) == sorted(expected)


@pytest.mark.parametrize('rule, expected', [
    ('light red bags contain 1 bright white bag, 2 muted yellow bags.\n',
     {
         'light red': [
             ('bright white', 1),
             ('muted yellow', 2)
         ]
     }),
    ('light red bags contain 1 bright white bag.\n',
     {
         'light red': [
            ('bright white', 1)
        ]
     }),
    ('faded blue bags contain no other bags.\n',
     {
         'faded blue': []
     })
])
def test_parse_rule_1(rule, expected):

    actual = parse_rule(rule)

    assert actual == expected


@pytest.mark.parametrize('color, expected', [
    ('shiny gold', 0),
    ('muted yellow', 11),
    ('light red', 26)
])
def test_count_contents(color, expected):
    ruleset = [
        {
            'light red': [
                ('bright white', 1),
                ('muted yellow', 2)
            ]
        },
        {
            'dark orange': [
                ('bright white', 3),
                ('muted yellow', 4)
            ]
        },
        {
            'bright white': [
                ('shiny gold', 1)
            ]
        },
        {
            'muted yellow': [
                ('shiny gold', 2),
                ('faded blue', 9)
            ]
        },
        {
            'shiny gold': []
        },
        {
            'faded blue': []
        }
    ]

    actual = count_contents(ruleset, color)

    assert actual == expected
