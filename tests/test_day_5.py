import pytest

from days.day_5 import *


@pytest.mark.parametrize('boarding_pass, expected', [
    ('FBFBBFFRLR', (44, 5)),
    ('BFFFBBFRRR', (70, 7)),
    ('FFFBBBFRRR', (14, 7)),
    ('BBFFBBFRLL', (102, 4))
])
def test_get_seat_coordinates(boarding_pass, expected):
    aircraft_dimensions = (128, 8)

    actual = get_seat_coordinates(boarding_pass, aircraft_dimensions)

    assert actual == expected


def test_binarise_boarding_pass():
    boarding_pass = 'FBFBBFFRLR'
    expected = '0101100101'

    actual = binarise_boarding_pass(boarding_pass)

    assert actual == expected


def test_get_coordinate():
    binary_seat = '0101100'
    dimension = 128
    expected = 44

    actual = get_coordinate(binary_seat, dimension)

    assert actual == expected


def test_get_seat_id():
    seat_coordinates = (44, 5)
    expected = 357

    actual = get_seat_id(seat_coordinates)

    assert actual == expected
