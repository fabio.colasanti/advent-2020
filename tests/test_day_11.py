import pytest

from days.day_11 import *


@pytest.mark.parametrize('seat, adjacent, expected', [
    ((3, 4), True, 2),
    ((3, 4), False, 8)
])
def test_count_occupied_seats_around(seat, adjacent, expected):
    layout = [['.', '.', '.', '.', '.', '.', '.', '#', '.'],
              ['.', '.', '.', '#', '.', '.', '.', '.', '.'],
              ['.', '#', '.', '.', '.', '.', '.', '.', '.'],
              ['.', '.', '.', '.', '.', '.', '.', '.', '.'],
              ['.', '.', '#', 'L', '.', '.', '.', '.', '#'],
              ['.', '.', '.', '.', '#', '.', '.', '.', '.'],
              ['.', '.', '.', '.', '.', '.', '.', '.', '.'],
              ['#', '.', '.', '.', '.', '.', '.', '.', '.'],
              ['.', '.', '.', '#', '.', '.', '.', '.', '.']]

    actual = count_occupied_seats_around(layout, seat, adjacent)

    assert actual == expected


def test_count_occupied_seats_around_2():
    seat = (1, 1)
    layout = [['.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
              ['.', 'L', '.', 'L', '.', '#', '.', '#', '.', '#', '.', '#', '.'],
              ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.']]

    expected = 0

    actual = count_occupied_seats_around(layout, seat, False)

    assert actual == expected


def test_change_state_changed():
    layout = [['#', '.', '#', '#', '.', 'L', '#', '.', '#', '#'],
              ['#', 'L', '#', '#', '#', 'L', 'L', '.', 'L', '#'],
              ['L', '.', '#', '.', '#', '.', '.', '#', '.', '.'],
              ['#', 'L', '#', '#', '.', '#', '#', '.', 'L', '#'],
              ['#', '.', '#', '#', '.', 'L', 'L', '.', 'L', 'L'],
              ['#', '.', '#', '#', '#', 'L', '#', '.', '#', '#'],
              ['.', '.', '#', '.', '#', '.', '.', '.', '.', '.'],
              ['#', 'L', '#', '#', '#', '#', '#', '#', 'L', '#'],
              ['#', '.', 'L', 'L', '#', '#', '#', 'L', '.', 'L'],
              ['#', '.', '#', 'L', '#', '#', '#', '.', '#', '#']]

    expected_new_layout = [['#', '.', '#', 'L', '.', 'L', '#', '.', '#', '#'],
                           ['#', 'L', 'L', 'L', '#', 'L', 'L', '.', 'L', '#'],
                           ['L', '.', 'L', '.', 'L', '.', '.', '#', '.', '.'],
                           ['#', 'L', 'L', 'L', '.', '#', '#', '.', 'L', '#'],
                           ['#', '.', 'L', 'L', '.', 'L', 'L', '.', 'L', 'L'],
                           ['#', '.', 'L', 'L', '#', 'L', '#', '.', '#', '#'],
                           ['.', '.', 'L', '.', 'L', '.', '.', '.', '.', '.'],
                           ['#', 'L', '#', 'L', 'L', 'L', 'L', '#', 'L', '#'],
                           ['#', '.', 'L', 'L', 'L', 'L', 'L', 'L', '.', 'L'],
                           ['#', '.', '#', 'L', '#', 'L', '#', '.', '#', '#']]

    actual_new_layout, changed = change_state(layout, True, 4)

    assert actual_new_layout == expected_new_layout
    assert changed is True


def test_change_state_changed_2():
    layout = [['#', '.', '#', '#', '.'],
              ['L', 'L', '#', '#', '#'],
              ['L', '.', '#', '.', '#']]

    expected_new_layout = [['#', '.', '#', 'L', '.'],
                           ['L', 'L', 'L', 'L', '#'],
                           ['#', '.', '#', '.', '#']]

    actual_new_layout, changed = change_state(layout, True, 4)

    assert actual_new_layout == expected_new_layout
    assert changed is True


def test_change_state_3():
    layout = [['#', '.', 'L', 'L', '.', 'L', 'L', '.', 'L', '#'],
              ['#', 'L', 'L', 'L', 'L', 'L', 'L', '.', 'L', 'L'],
              ['L', '.', 'L', '.', 'L', '.', '.', 'L', '.', '.'],
              ['L', 'L', 'L', 'L', '.', 'L', 'L', '.', 'L', 'L'],
              ['L', '.', 'L', 'L', '.', 'L', 'L', '.', 'L', 'L'],
              ['L', '.', 'L', 'L', 'L', 'L', 'L', '.', 'L', 'L'],
              ['.', '.', 'L', '.', 'L', '.', '.', '.', '.', '.'],
              ['L', 'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L', '#'],
              ['#', '.', 'L', 'L', 'L', 'L', 'L', 'L', '.', 'L'],
              ['#', '.', 'L', 'L', 'L', 'L', 'L', '.', 'L', '#']]

    expected_new_layout = [['#', '.', 'L', '#', '.', '#', '#', '.', 'L', '#'],
                           ['#', 'L', '#', '#', '#', '#', '#', '.', 'L', 'L'],
                           ['L', '.', '#', '.', '#', '.', '.', '#', '.', '.'],
                           ['#', '#', 'L', '#', '.', '#', '#', '.', '#', '#'],
                           ['#', '.', '#', '#', '.', '#', 'L', '.', '#', '#'],
                           ['#', '.', '#', '#', '#', '#', '#', '.', '#', 'L'],
                           ['.', '.', '#', '.', '#', '.', '.', '.', '.', '.'],
                           ['L', 'L', 'L', '#', '#', '#', '#', 'L', 'L', '#'],
                           ['#', '.', 'L', '#', '#', '#', '#', '#', '.', 'L'],
                           ['#', '.', 'L', '#', '#', '#', '#', '.', 'L', '#']]

    actual_new_layout, changed = change_state(layout, False, 5)

    assert actual_new_layout == expected_new_layout
    assert changed is True


def test_change_state_unchanged():
    layout = [['#', '.', '#', 'L', '.', 'L', '#', '.', '#', '#'],
              ['#', 'L', 'L', 'L', '#', 'L', 'L', '.', 'L', '#'],
              ['L', '.', '#', '.', 'L', '.', '.', '#', '.', '.'],
              ['#', 'L', '#', '#', '.', '#', '#', '.', 'L', '#'],
              ['#', '.', '#', 'L', '.', 'L', 'L', '.', 'L', 'L'],
              ['#', '.', '#', 'L', '#', 'L', '#', '.', '#', '#'],
              ['.', '.', 'L', '.', 'L', '.', '.', '.', '.', '.'],
              ['#', 'L', '#', 'L', '#', '#', 'L', '#', 'L', '#'],
              ['#', '.', 'L', 'L', 'L', 'L', 'L', 'L', '.', 'L'],
              ['#', '.', '#', 'L', '#', 'L', '#', '.', '#', '#']]

    new_layout, changed = change_state(layout, True, 4)

    assert new_layout == layout
    assert changed is False


def test_reassign_seats_old_rules():
    layout = [['#', '.', '#', '#', '.', '#', '#', '.', '#', '#'],
              ['#', '#', '#', '#', '#', '#', '#', '.', '#', '#'],
              ['#', '.', '#', '.', '#', '.', '.', '#', '.', '.'],
              ['#', '#', '#', '#', '.', '#', '#', '.', '#', '#'],
              ['#', '.', '#', '#', '.', '#', '#', '.', '#', '#'],
              ['#', '.', '#', '#', '#', '#', '#', '.', '#', '#'],
              ['.', '.', '#', '.', '#', '.', '.', '.', '.', '.'],
              ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#'],
              ['#', '.', '#', '#', '#', '#', '#', '#', '.', '#'],
              ['#', '.', '#', '#', '#', '#', '#', '.', '#', '#']]

    expected_new_layout = [['#', '.', '#', 'L', '.', 'L', '#', '.', '#', '#'],
                           ['#', 'L', 'L', 'L', '#', 'L', 'L', '.', 'L', '#'],
                           ['L', '.', '#', '.', 'L', '.', '.', '#', '.', '.'],
                           ['#', 'L', '#', '#', '.', '#', '#', '.', 'L', '#'],
                           ['#', '.', '#', 'L', '.', 'L', 'L', '.', 'L', 'L'],
                           ['#', '.', '#', 'L', '#', 'L', '#', '.', '#', '#'],
                           ['.', '.', 'L', '.', 'L', '.', '.', '.', '.', '.'],
                           ['#', 'L', '#', 'L', '#', '#', 'L', '#', 'L', '#'],
                           ['#', '.', 'L', 'L', 'L', 'L', 'L', 'L', '.', 'L'],
                           ['#', '.', '#', 'L', '#', 'L', '#', '.', '#', '#']]

    actual_new_layout = reassign_seats(layout, True, 4)

    assert actual_new_layout == expected_new_layout


def test_reassign_seats_new_rules():
    layout = [['#', '.', '#', '#', '.', '#', '#', '.', '#', '#'],
              ['#', '#', '#', '#', '#', '#', '#', '.', '#', '#'],
              ['#', '.', '#', '.', '#', '.', '.', '#', '.', '.'],
              ['#', '#', '#', '#', '.', '#', '#', '.', '#', '#'],
              ['#', '.', '#', '#', '.', '#', '#', '.', '#', '#'],
              ['#', '.', '#', '#', '#', '#', '#', '.', '#', '#'],
              ['.', '.', '#', '.', '#', '.', '.', '.', '.', '.'],
              ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#'],
              ['#', '.', '#', '#', '#', '#', '#', '#', '.', '#'],
              ['#', '.', '#', '#', '#', '#', '#', '.', '#', '#']]

    expected_new_layout = [['#', '.', 'L', '#', '.', 'L', '#', '.', 'L', '#'],
                           ['#', 'L', 'L', 'L', 'L', 'L', 'L', '.', 'L', 'L'],
                           ['L', '.', 'L', '.', 'L', '.', '.', '#', '.', '.'],
                           ['#', '#', 'L', '#', '.', '#', 'L', '.', 'L', '#'],
                           ['L', '.', 'L', '#', '.', 'L', 'L', '.', 'L', '#'],
                           ['#', '.', 'L', 'L', 'L', 'L', '#', '.', 'L', 'L'],
                           ['.', '.', '#', '.', 'L', '.', '.', '.', '.', '.'],
                           ['L', 'L', 'L', '#', '#', '#', 'L', 'L', 'L', '#'],
                           ['#', '.', 'L', 'L', 'L', 'L', 'L', '#', '.', 'L'],
                           ['#', '.', 'L', '#', 'L', 'L', '#', '.', 'L', '#']]

    actual_new_layout = reassign_seats(layout, False, 5)

    assert actual_new_layout == expected_new_layout


def test_count_seats_by_type():
    seat_type = '#'
    layout = [['#', '.', '#', 'L', '.', 'L', '#', '.', '#', '#'],
              ['#', 'L', 'L', 'L', '#', 'L', 'L', '.', 'L', '#'],
              ['L', '.', '#', '.', 'L', '.', '.', '#', '.', '.'],
              ['#', 'L', '#', '#', '.', '#', '#', '.', 'L', '#'],
              ['#', '.', '#', 'L', '.', 'L', 'L', '.', 'L', 'L'],
              ['#', '.', '#', 'L', '#', 'L', '#', '.', '#', '#'],
              ['.', '.', 'L', '.', 'L', '.', '.', '.', '.', '.'],
              ['#', 'L', '#', 'L', '#', '#', 'L', '#', 'L', '#'],
              ['#', '.', 'L', 'L', 'L', 'L', 'L', 'L', '.', 'L'],
              ['#', '.', '#', 'L', '#', 'L', '#', '.', '#', '#']]

    expected = 37

    actual = count_seats_by_type(layout, seat_type)

    assert actual == expected
