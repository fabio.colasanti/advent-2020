from days.day_2 import get_valid_passwords_old_system, get_valid_passwords_new_system


def test_get_valid_passwords_old_system():
    passwords = ['1-3 a: abcde',
                 '1-3 b: cdefg',
                 '2-9 c: ccccccccc']
    expected = ['abcde', 'ccccccccc']

    actual = get_valid_passwords_old_system(passwords)

    assert actual == expected


def test_get_valid_passwords_new_system():
    passwords = ['1-3 a: abcde',
                 '1-3 b: cdefg',
                 '2-9 c: ccccccccc']
    expected = ['abcde']

    actual = get_valid_passwords_new_system(passwords)

    assert actual == expected
