import pytest

from days.day_6 import *


@pytest.mark.parametrize('group, expected', [
    ('abc', 3),
    ('a b c', 3),
    ('ab ac', 3),
    ('a a a a', 1),
    ('b', 1)
])
def test_count_yes_from_anyone_group(group, expected):
    actual = count_yes_from_anyone_group(group)

    assert actual == expected


@pytest.mark.parametrize('group, expected', [
    ('abc', 3),
    ('a b c', 0),
    ('ab ac', 1),
    ('a a a a', 1),
    ('b', 1),
    ('aaa ba', 1)
])
def test_count_yes_from_everyone_group(group, expected):
    actual = count_yes_from_everyone_group(group)

    assert actual == expected
