import pytest

from days.day_3 import *


@pytest.mark.parametrize('area_width, area_height, step_x, step_y, expected', [
    (11, 11, 3, 1, 3),
    (11, 11, 3, 2, 2),
    (11, 11, 5, 1, 5)
])
def test_replicas_needed(area_width, area_height, step_x, step_y, expected):
    actual = replicas_needed(area_width, area_height, step_x, step_y)

    assert actual == expected


def test_build_terrain():
    replicas = 2
    area = [
        '..##.......',
        '#...#...#..',
        '.#....#..#.',
    ]
    expected = [
        '..##.........##.......',
        '#...#...#..#...#...#..',
        '.#....#..#..#....#..#.',
    ]

    actual = build_terrain(area, replicas)

    assert actual == expected


def test_get_trajectory():
    terrain_length = 11
    step_x = 3
    step_y = 1
    expected = [(0, 0), (3, 1), (6, 2), (9, 3), (12, 4), (15, 5), (18, 6), (21, 7), (24, 8), (27, 9), (30, 10)]

    actual = get_trajectory(terrain_length, step_x, step_y)

    assert actual == expected


def test_get_obstacles():
    terrain = [
        '..##.......',
        '#...#...#..',
        '.#....#..#.',
    ]
    expected = [(2, 0), (3, 0), (0, 1), (4, 1), (8, 1), (1, 2), (6, 2), (9, 2)]

    actual = get_obstacles(terrain)

    assert actual == expected


def test_get_obstacles_on_trajectory():
    slope = (3, 1)
    area = [
        '..##.......',
        '#...#...#..',
        '.#....#..#.',
        '..#.#...#.#',
        '.#...##..#.',
        '..#.##.....',
        '.#.#.#....#',
        '.#........#',
        '#.##...#...',
        '#...##....#',
        '.#..#...#.#',
    ]
    expected = [(6, 2), (12, 4), (15, 5), (21, 7), (24, 8), (27, 9), (30, 10)]

    actual = get_obstacles_on_trajectory(area, slope)

    assert actual == expected
