import pytest

from days.day_10 import *


def test_get_adapters_diffs():
    ratings = [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4]

    expected = [1, 3, 1, 1, 1, 3, 1, 1, 3, 1, 3]

    actual = get_adapters_diffs(ratings, 0)

    assert expected == actual


@pytest.mark.parametrize('sequence, expected', [
    ([1, 3, 1, 1, 1, 1, 3], [[1], [1, 1, 1, 1]]),
    ([1, 3, 1, 1, 1, 3, 1, 1, 3, 1, 3], [[1], [1, 1, 1], [1, 1], [1]]),
])
def test_get_chuncks(sequence, expected):
    actual = get_chuncks(sequence)

    assert actual == expected


@pytest.mark.parametrize('n, expected', [
    (3, 4),
    (4, 7),
    (10, 274)
])
def test_tribonacci(n, expected):
    actual = tribonacci(n)

    assert actual == expected


@pytest.mark.parametrize('adapters_diffs, expected', [
    ([1, 3, 1, 1, 1, 1, 3], 7),
    ([1, 3, 1, 1, 1, 3, 1, 1, 3, 1, 3], 8),
    ([1, 1, 1, 1, 3, 1, 1, 1, 1, 3, 3, 1, 1, 1, 3, 1, 1, 3, 3, 1, 1, 1, 1, 3, 1], 2744),
    ([1, 1, 1, 1, 3, 1, 1, 1, 1, 3, 3, 1, 1, 1, 3, 1, 1, 3, 3, 1, 1, 1, 1, 3, 1, 3, 3, 1, 1, 1, 1, 3], 19208)
])
def test_get_combinations(adapters_diffs, expected):
    actual = get_combinations(adapters_diffs)

    assert actual == expected
