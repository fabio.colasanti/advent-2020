import pytest

from days.day_15 import number_at_turn


@pytest.mark.parametrize('seq, turn, expected', [
    ([0, 3, 6], 4, 0),
    ([0, 3, 6], 5, 3),
    ([0, 3, 6], 6, 3),
    ([0, 3, 6], 7, 1),
    ([0, 3, 6], 8, 0),
    ([0, 3, 6], 2020, 436),
    ([1, 3, 2], 2020, 1),
    ([2, 1, 3], 2020, 10),
    ([1, 2, 3], 2020, 27),
    ([2, 3, 1], 2020, 78),
    ([3, 2, 1], 2020, 438),
    ([3, 1, 2], 2020, 1836),
])
def test_number_at_turn(seq, turn, expected):
    actual = number_at_turn(seq, turn)

    assert actual == expected
