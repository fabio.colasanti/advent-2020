import pytest

from days.day_12 import Ship, Waypoint, move


@pytest.mark.parametrize('position, orientation, value, expected', [
    ((0, 0), 'E', 10, ((10, 0), 'E')),
    ((5, 0), 'S', 10, ((5, -10), 'S')),
    ((5, 0), 'N', 10, ((5, 10), 'N')),
    ((-5, -1), 'W', 10, ((-15, -1), 'W'))
])
def test_move_ship_forward(position, orientation, value, expected):
    ship = Ship(position, orientation)

    ship.shift(orientation, value)

    assert ship.position_x == expected[0][0]
    assert ship.position_y == expected[0][1]
    assert ship.orientation == expected[1]


@pytest.mark.parametrize('position, orientation, rotation, value, expected', [
    ((0, 0), 'E', 'R', 90, ((0, 0), 'S')),
    ((0, 0), 'E', 'R', 180, ((0, 0), 'W')),
    ((0, 0), 'W', 'R', 360, ((0, 0), 'W')),
    ((0, 0), 'W', 'R', 90, ((0, 0), 'N')),
    ((0, 0), 'E', 'R', 540, ((0, 0), 'W')),
    ((0, 0), 'N', 'R', 0, ((0, 0), 'N')),
    ((0, 0), 'W', 'L', 90, ((0, 0), 'S')),
    ((0, 0), 'N', 'L', 90, ((0, 0), 'W')),
    ((0, 0), 'S', 'L', 450, ((0, 0), 'E')),
])
def test_rotate_ship(position, orientation, rotation, value, expected):
    ship = Ship(position, orientation)

    ship.rotate(rotation, value)

    assert ship.position_x == expected[0][0]
    assert ship.position_y == expected[0][1]
    assert ship.orientation == expected[1]


@pytest.mark.parametrize('position, orientation, direction, value, expected', [
    ((0, 0), 'E', 'E', 10, ((10, 0), 'E')),
    ((0, 0), 'E', 'W', 10, ((-10, 0), 'E')),
    ((0, 0), 'E', 'N', 10, ((0, 10), 'E')),
    ((-5, -1), 'W', 'S', 10, ((-5, -11), 'W'))
])
def test_shift_ship(position, orientation, direction, value, expected):
    ship = Ship(position, orientation)

    ship.shift(direction, value)

    assert ship.position_x == expected[0][0]
    assert ship.position_y == expected[0][1]
    assert ship.orientation == expected[1]


@pytest.mark.parametrize('position, orientation, command, expected', [
    ((0, 0), 'E', 'F10', ((10, 0), 'E')),
    ((10, 0), 'E', 'N3', ((10, 3), 'E')),
    ((10, 3), 'E', 'F7', ((17, 3), 'E')),
    ((17, 3), 'E', 'R90', ((17, 3), 'S')),
    ((17, 3), 'S', 'F11', ((17, -8), 'S')),
])
def test_move_no_waypoint(position, orientation, command, expected):
    ship = Ship(position, orientation)

    move(ship, command)

    assert ship.position_x == expected[0][0]
    assert ship.position_y == expected[0][1]
    assert ship.orientation == expected[1]


@pytest.mark.parametrize('ship_position, waypoint_position, command, expected_ship_position, expected_waypoint_position', [
    ((0, 0), (10, 1), 'F10', (100, 10), (10, 1)),
    ((100, 10), (10, 1), 'N3', (100, 10),  (10, 4)),
    ((100, 10), (10, 4), 'F7', (170, 38),  (10, 4)),
    ((170, 38), (10, 4), 'R90', (170, 38), (4, -10)),
    ((170, 38), (4, -10), 'F11', (214, -72), (4, -10)),
])
def test_move_with_waypoint(ship_position, waypoint_position, command, expected_ship_position, expected_waypoint_position):
    ship = Ship(ship_position, 'E')
    waypoint = Waypoint(waypoint_position)

    move(ship, command, waypoint)

    assert ship.position_x == expected_ship_position[0]
    assert ship.position_y == expected_ship_position[1]
    assert waypoint.position_x == expected_waypoint_position[0]
    assert waypoint.position_y == expected_waypoint_position[1]
